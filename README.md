# Erloom - Logger

This package used with Filebeat on your server to generate log file within *Laravel 6.x and above*. Logging file later will be consumed to Logstash for logging purpose.

The feature of this package :
- Convert Laravel Log into JSON formatting using channel `dailybeat`
- Create Laravel Endpoint Log into JSON formatting using channel `monitor`
- Create new Middleware alias for channel `monitor` to implement on your REST API, with a Middleware alias named `request.log`

## Installation

- Install with `composer require erloom/logger`, later you can use the 2 features available separately based on the installation instructions.

### Change Usual log into JSON + Daily format
- To change your usual logging into JSON, change your `LOG_CHANNEL` into `dailybeat`
```
LOG_CHANNEL=dailybeat
...
```
- Later, your logs will be generated at `storage/logs` with this format :
```
- laravel-2021-03-04.log
- laravel-2021-03-05.log
...
```
with this content :
```json
{"@timestamp":"2021-03-04T11:20:36.053490+00:00","@version":1,"host":"xxx.local","message":"Test","type":"Laravel","channel":"local","level":"ERROR","monolog_level":400}
```

### Log your REST API
- To log your endpoint, the package automatically register middleware called `request.log` in your application. You can use it within your API routes.
```php
Route::get('/test', function (Request $request) {
    return 'Test';
})->middleware('request.log');
```

- Later, your logs will be generated at `storage/logs` with this format :
```
- monitor-2021-03-04.log
- monitor-2021-03-05.log
...
```
with this content :
```json
{"@timestamp":"2021-10-04T09:37:09.348133+00:00","@version":1,"host":"xxx.local","message":"request_monitor","type":"Laravel","channel":"local","level":"INFO","monolog_level":200,"context":{"url":"YOUR_API_ROUTE","controller":null,"action":"Closure","request_method":"GET","ip_address":"REQUESTER_IP","user_agent":"USER_AGENT","request_body":"","response_status":200,"response_body":"Test","duration":18.95594596862793}}
```