<?php

namespace Erloom\Logger\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class RequestLogMiddleware
{

    public function handle($request, Closure $next)
    {
        $response = $next($request);

        Log::channel('monitor')->info("request_monitor", [
            'url' => $request->getUri(),
            'controller' => $request->route() !== null && $request->route()->getActionName() !== 'Closure' ? get_class($request->route()->getController()) : null,
            'action' => $request->route() !== null ? $request->route()->getActionMethod() : null,
            'request_method' => $request->getMethod(),
            'ip_address' => $request->ip(),
            'user_agent' => $request->userAgent(),
            'request_body' => $request->getContent(),
            'response_status' => $response->getStatusCode(),
            'response_body' => $response->getContent(),
            'duration' => (microtime(true) - LARAVEL_START) * 1000
        ]);

        return $response;
    }
    
}