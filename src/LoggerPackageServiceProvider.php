<?php

namespace Erloom\Logger;

use Illuminate\Support\ServiceProvider;
use Erloom\Logger\Http\Middleware\RequestLogMiddleware;
use Illuminate\Routing\Router;

class LoggerPackageServiceProvider extends ServiceProvider
{
  public function register() {
      
  }

  public function boot() {

    //ADD NEW CHANNEL FOR LOG CALLED 'dailybeat'//
    $this->app->make('config')->set('logging.channels.dailybeat', [
        'driver' => 'daily',
        'formatter' => \Monolog\Formatter\LogstashFormatter::class,
        'formatter_with' => [
            'applicationName' => config('app.name'),
        ],
        'path' => storage_path('logs/laravel.log'),
        'level' => env('LOG_LEVEL', 'debug'),
        'days' => env('LOGGER_LOG_DAYS', 14),
    ]);

    //ADD NEW CHANNEL FOR MIDDLEWARE LOG CALLED 'monitor'//
    $this->app->make('config')->set('logging.channels.monitor', [
        'driver' => 'daily',
        'formatter' => \Monolog\Formatter\LogstashFormatter::class,
        'formatter_with' => [
            'applicationName' => config('app.name'),
        ],
        'path' => storage_path('logs/monitor.log'),
        'level' => env('LOG_LEVEL', 'debug'),
        'days' => env('LOGGER_LOG_DAYS', 14),
    ]);

    //REGISTER MIDDLEWARE TO WHOLE APPLICATION//
    $router = $this->app->make(Router::class);
    $router->aliasMiddleware('request.log', RequestLogMiddleware::class);

  }

}
